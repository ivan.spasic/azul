#!/usr/bin/python3

import argparse
import string
import random

parser = argparse.ArgumentParser()

parser.add_argument("-m", "--manual", help="script will ask to continue after  piece is placed", default=False, action="store_true")

arguments = parser.parse_args()

fig_type=['A', 'B', 'C', 'D', 'E']
board=[ [0]*5 for i in range(5)]

points=0
bonus_points=0

def bonus_points_check():
    global bonus_points
    global board

    # check for filled rows 2 points
    for x in board:
        if 0 not in x:
            bonus_points += 2

    # check for filled columns
    for x in range(0,5):
        try:
            for y in range(0,5):
                if board[y][x] == 0:
                    raise
        except:
            pass
        else:
            bonus_points += 7
        
    
    # check if figure's appear in every row
    for fig in fig_type:
        try:
            for x in range(0, 5):
                if fig not in board[x]:
                    raise
        except:
            pass
        else:
            bonus_points += 10


def new_fig_points(row, clmn):
    global board
    global points

    row_points = 0
    clmn_points = 0

    # row points
    for x in range(clmn, 5, 1):
        if board[row][x] != 0:
            row_points += 1
        else:
            break

    for x in range(clmn-1, -1, -1):
        if board[row][x] != 0:
            row_points += 1
        else:
            break

    # column points
    for y in range(row+1, 5, 1):

        if board[y][clmn] != 0:
            clmn_points += 1
        else:
            break
    for y in range(row-1, -1, -1):

        if board[y][clmn] != 0:
            clmn_points += 1
        else:
            break

    points += (row_points + clmn_points)

    print("New fig points:", (row_points + clmn_points))

        

def random_fig():
    '''
    return False if figure is already present in row or if spot is occupied
    return True if successfuly 
    '''
    global board

    # ?1 can 2 same figures be picked ?

    # random pick of a figure type
    fig = random.choice(fig_type)

    # random pick of a row 
    row = random.choice(range(0,5))
    # check if figure is already present in that row
    for x in board[row]:
        if x == fig:
            print("Row duplicate, pick again")
            return False
    
    clmn = random.choice(range(0,5))

    # ?3 check if there is already figure on that spot
    if board[row][clmn] != 0:
        print("Occupied spot, pick again")
        return False

    # Put the figure on to the board
    board[row][clmn] = fig
    new_fig_points(row, clmn)

    return True
   
def turn():
    global board
    global arguments
    global points

    # pick 3 random figures
    for x in range(0,3):
        while not(random_fig()):
            pass
        
        for x in board:
            for y in x:
                print( ' {} '.format(y), end="")
            print('')

        print("Total points: ", points)
        # check the points scored after placing figure

        if arguments.manual:
            input("Press Enter to spawn another figure...")


def end_check():
    global board

    for row in board:
        if 0 not in row:
            # row is filed with figures
            return 1
    
    return 0

def main():
    global arguments


    while True:
        turn()

        # check if the game has ended
        end = end_check()

        if end:
            # bonus points calculation
            print("\n----- The end -----\n")
            bonus_points_check()

            # print score
            print("Points throughout the game: ", points)
            print("Bonus point: ", bonus_points)
            print("Final score: ", (bonus_points+points))

            # print final board
            print("Final board:")

            for x in board:
                for y in x:
                    print( ' {} '.format(y), end="")
                print('')


            exit(0)

        if arguments.manual:
            input("Press Enter for the next turn...")

if __name__ == "__main__":
    main()
